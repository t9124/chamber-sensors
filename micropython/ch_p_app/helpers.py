import network
import os

def do_connect():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('Łączę z siecią...')
        wlan.connect('ihell', 'K1nkw9st269')
        while not wlan.isconnected():
            pass
    print('Konfiguracja sieci:', wlan.ifconfig())
    
def check_connection(ip_to_check):
    result = os.popen('ping -n 1 ' + ip_to_check)
    if 'TTL' in result:
        print('Połączono:', result)

do_connect()
check_connection('192.168.1.1')