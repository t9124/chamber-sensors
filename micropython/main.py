# import math

# celsius = 20
# speed_of_sound = 331.3 * math.sqrt(1 + (celsius / 273.15))
# print(speed_of_sound)

# Complete project details at https://RandomNerdTutorials.com/micropython-hc-sr04-ultrasonic-esp32-esp8266/
from hcsr04 import HCSR04
from time import sleep

# ESP32
sensor = HCSR04(trigger_pin=5, echo_pin=18, echo_timeout_us=10000)

# ESP8266
#sensor = HCSR04(trigger_pin=12, echo_pin=14, echo_timeout_us=10000)

for count in range(10):
    distance = sensor.distance_cm()
    print('Distance:', distance, 'cm')
    sleep(1)