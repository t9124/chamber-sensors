import machine
import dht
import time
import os

sensor_dht = dht.DHT22(machine.Pin(14))

def save_to_file():
    with open('log.txt', 'a') as file:
        to_file = str(now) + ';' + str(temp) + ';' + str(humi)
        file.write(to_file + '\n')    

while True:
    
    now = time.gmtime()[:6] # save the measurement time
    #os.system('cls')

    print('*' * 5)
    sensor_dht.measure() # measurement
    temp = sensor_dht.temperature()
    humi = sensor_dht.humidity()
    
    save_to_file()
    
    print(now)
    print(temp)
    print(humi)
    
    time.sleep_ms(2000)