from random import choices
from unittest.util import _MAX_LENGTH
from django import forms

sensors_list = [('AM2302', 'Czyjnik AM2302'), ('AJ-SR04M', 'Czujnik AJ-SR04M')]

class MeasureForm(forms.Form):
    form_measure = forms.DecimalField(label='Ile ostatnich zapisów wyświetlić?', required=False, max_digits=4, min_value=1)
    # start_date = forms.DateField()
    # end_date = forms.DateField()
    # sensor_name = forms.ChoiceField(label='Czujnik', choices=sensors_list)


class ChartsForm(forms.Form):
    # charts_type = forms.ChoiceField(label='Rodzaj wykresu', choices=[('line_chart', 'Wykres liniowy'), ('other_chart', "Inny wykres")])
    form_measure = forms.DecimalField(label='Wykres dla ilu zapisów?', required=False, max_digits=4, min_value=1)
    # start_date = forms.DateField()
    # end_date = forms.DateField()
    # sensor_name = forms.ChoiceField(label='Czujnik', choices=sensors_list)