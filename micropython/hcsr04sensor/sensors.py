import time
import math
# import warnings
import machine

time_passed = 0
sonar_signal_on = 0
sonar_signal_off = 0

def basic_distance(celsius=20):
    """Return an unformatted distance in cm's as read directly from
    RPi.GPIO."""

    speed_of_sound = 331.3 * math.sqrt(1 + (celsius / 273.15))
    trig_pin = machine.Pin(33, machine.Pin.OUT)
    echo_pin = machine.Pin(27, machine.Pin.IN)
    trig_pin.off()
    time.sleep(0.1)
    trig_pin.on()
    time.sleep(0.00001)
    trig_pin.off()
    echo_status_counter = 1
    while echo_pin == 0:
        if echo_status_counter < 1000:
            sonar_signal_off = time.time()
            echo_status_counter += 1
        else:
            print("Echo pulse was not received")
    while echo_pin == 1:
        sonar_signal_on = time.time()
    time_passed = sonar_signal_on - sonar_signal_off
    return time_passed * ((speed_of_sound * 100) / 2)
print(basic_distance())