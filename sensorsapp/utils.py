import matplotlib.pyplot as plt
import base64
from io import BytesIO

from sympy import rotations

def get_graph():
    buffer = BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    image_png = buffer.getvalue()
    graph = base64.b64encode(image_png)
    graph = graph.decode('utf-8')
    buffer.close()
    return graph

def get_plot(x,y,z):
    plt.switch_backend('AGG')
    plt.figure(figsize=(8,5))
    plt.title('Wykres temperatury i wilgotności')
    plt.plot(x,y)
    plt.plot(x,z)
    plt.xticks(rotation=45)
    plt.xlabel('Data pomiaru')
    plt.ylabel('Wartość')
    plt.tight_layout()
    graph = get_graph()
    return graph