from crypt import methods
from urllib import response
from django.shortcuts import render
from .models import Measurements
import json
from django.http import HttpResponse
from django.template import loader
import matplotlib.pyplot as plt
from django.views.decorators.csrf import csrf_exempt
from pprint import pprint
from .utils import get_plot
from .forms import ChartsForm, MeasureForm

def charts(request):
    form = ChartsForm()
    if request.method == 'POST':
        form = ChartsForm(request.POST)
        measures_numbers = int(request.POST['form_measure'])
    else:
        measures_numbers = 20
    measures = Measurements.objects.order_by('-created_datetime')[:measures_numbers]
    x = [x.created_datetime for x in measures]
    y = [y.sensor_temperature for y in measures]
    z = [z.sensor_humidity for z in measures]
    chart = get_plot(x, y, z)
    return render(request, 'sensorsapp/charts.html', 
                            {'measures': measures,
                            'chart': chart,
                            "form": form})

def tabels(request):
    form = MeasureForm()
    if request.method == 'POST':
        form = MeasureForm(request.POST)
        if request.POST['form_measure']:
            measures_numbers = int(request.POST['form_measure'])
        else:
            measures_numbers = 10
    else:
        measures_numbers = 10
    measures = Measurements.objects.order_by('-created_datetime')[:measures_numbers]
    return render(request, 'sensorsapp/tabels.html', 
                            {'measures': measures,
                            "form": form})

@csrf_exempt
def recieve_data(request):
    if request.method == 'POST':
        data=json.loads(request.body)
        Measurements.objects.register_from_data(data)
    else:
        pprint(request)
        print('Connection error!')
        
    response = HttpResponse('Pomiar')
    response.set_cookie('Dane', 'Basen')
    return response