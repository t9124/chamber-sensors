import network

def lan_connect():
    sta_if = network.WLAN(network.STA_IF)
    while not sta_if.isconnected():
        print('connecting to network...')
        continue
    print('connected')
    sta_if.active(True)
    sta_if.ifconfig(('192.168.1.66','255.255.255.0','192.168.1.1','8.8.8.8'))
    sta_if.connect('ihell', 'K1nkw9st269')
    print('network config:', sta_if.ifconfig())

lan_connect()


# now use usocket as usual
import usocket as socket
addr = socket.getaddrinfo('192.168.1.10', 5000)[0][-1]
print(addr)
s = socket.socket()
print(s)
s.connect(addr)
s.send(b'GET / HTTP/1.1\r\nHost: 192.168.1.10\r\n\r\n')
data = s.recv(3000)
print(data)
s.close()

