import network
import time
import dht
import machine
import urequests
import ujson

API_ENDPOINT = 'http://192.168.1.10:8000/get_data/'

def lan_connect():
    sta_if = network.WLAN(network.STA_IF)
    while not sta_if.isconnected():
        print('connecting to network...')
        time.sleep(1)
        continue
    print('connected')
    sta_if.active(True)
    sta_if.ifconfig(('192.168.1.20','255.255.255.0','192.168.1.1','8.8.8.8'))
    sta_if.connect('ihell', 'K1nkw9st269')
    print('network config:', sta_if.ifconfig())

def read_sensors():
    d = dht.DHT22(machine.Pin(14))
    d.measure()
    t = d.temperature() # eg. 23.6 (°C)
    h = d.humidity()    # eg. 41.3 (% RH)
    data = {
        'sensor_name': 'AM2302',
        'sensor_temperature': t,
        'sensor_humidity': h
        }
    data = ujson.dumps(data)
    headers = {'user-agent': 'my-app/0.0.1'}
    try:
        # result = urequests.post('https://httpbin.org/post', data=data)
        result = urequests.post('http://192.168.1.10:8000/get_data/', headers=headers, data=data)
        print(result.status_code)
        result.close()
    except:
        print('error')
    return data

lan_connect()

while True:
    sensors_data = read_sensors()
    print(sensors_data)
    print('*' * 80)
    time.sleep(3)

