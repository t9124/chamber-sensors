import network

def lan_connect():
    sta_if = network.WLAN(network.STA_IF)
    while not sta_if.isconnected():
        print('connecting to network...')
        continue
    print('connected')
    sta_if.active(True)
    sta_if.ifconfig(('192.168.43.50','255.255.255.0','192.168.43.1','8.8.8.8'))
    sta_if.connect('ihellPro', 'test1234')
    print('network config:', sta_if.ifconfig())

lan_connect()