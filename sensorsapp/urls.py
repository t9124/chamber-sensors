from django.urls import path
from . import views

urlpatterns = [
    path('', views.charts, name='charts'),
    path('tabels/', views.tabels, name='tabels'),
    path('get_data/', views.recieve_data, name='get_data'),
]
