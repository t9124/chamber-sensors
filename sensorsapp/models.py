from django.conf import settings
from django.db import models


class MeasurementsManager(models.Manager):

    def register_from_data(self, data):
        self.create(
            sensor_name=data['sensor_name'],
            sensor_humidity=data['sensor_humidity'],
            sensor_temperature=data['sensor_temperature']
        )


class Measurements(models.Model):

    created_datetime = models.DateTimeField(
        auto_now_add=True,
        )
    sensor_name = models.CharField(
        max_length=40,
        # editable = False
        )
    sensor_humidity = models.FloatField(
        max_length=10,
        # editable = False
        )
    sensor_temperature = models.FloatField(
        max_length=10,
        # editable = False
        )

    objects = MeasurementsManager()
